from __future__ import print_function
from urllib.parse import unquote_plus
from aes import *
from util import *
from blockfeeder import *

import json
import urllib
import boto3
import linecache
import sys
import socket

import base64
import hashlib

from botocore.client import Config
from botocore.exceptions import ClientError

def get_config(id: int):
    dynamodb = boto3.resource('dynamodb')
    if dynamodb:
        table = dynamodb.Table('SSP_FEP_config')
        id = table.get_item(Key={'ConfigID':id})
        config = id['Item']
        return config
    else :
        raise Exception("Unable to get Config")
        return None;

def save_config(id: int, config:list):
    dynamodb = boto3.resource('dynamodb')
    if dynamodb:
        table = dynamodb.Table('SSP_FEP_config')
        response = table.put_item(Item=config)
        return response
    else :
        raise Exception("Unable to get Config")
        return None;

def broadcast_data(ip_address:str, port:int, data: bytes):
    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    hostname = socket.gethostname()
    myip = socket.gethostbyname(hostname)
    print("From:"+myip)
    
    conn = s.connect((ip_address, port))
    print("Connected on "+ip_address+":"+str(port))
    rc = s.sendall(data)
    return rc

s3 = boto3.client('s3', config=Config(signature_version='s3v4'))
secretsmanager = boto3.client(
                service_name='secretsmanager',
                region_name="us-gov-west-1"
    )
def getEncKey():
    # See https://docs.aws.amazon.com/secretsmanager/latest/apireference/API_GetSecretValue.html
    try:
        print("Getting key")
        secret_name = "test/VPMBeaconKey"
        get_secret_value_response = secretsmanager.get_secret_value(
            SecretId=secret_name
        )
        print("Got key")

    except ClientError as e:
        if e.response['Error']['Code'] == 'DecryptionFailureException':
            # Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            # Deal with the exception here, and/or rethrow at your discretion.
            print(e.response)
            raise e
        elif e.response['Error']['Code'] == 'InternalServiceErrorException':
            # An error occurred on the server side.
            # Deal with the exception here, and/or rethrow at your discretion.
            print(e.response)
            raise e
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            # You provided an invalid value for a parameter.
            # Deal with the exception here, and/or rethrow at your discretion.
            print(e.response)
            raise e
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            # You provided a parameter value that is not valid for the current state of the resource.
            # Deal with the exception here, and/or rethrow at your discretion.
            print(e.response)
            raise e
        elif e.response['Error']['Code'] == 'ResourceNotFoundException':
            # We can't find the resource that you asked for.
            # Deal with the exception here, and/or rethrow at your discretion.
            print(e.response)
            raise e
        else:
            print(e.response)
            raise e
    else:
        # Decrypts secret using the associated KMS CMK.
        # Depending on whether the secret is a string or binary, one of these fields will be populated.
        if 'SecretString' in get_secret_value_response:
            enckey = json.loads(get_secret_value_response['SecretString'])['VPMBeaconKey']
            enckey = bytes.fromhex(enckey)
        else:
            enckey = base64.b64decode(get_secret_value_response['SecretBinary'])
            print("binary secret")
    return enckey        
            
def lambda_handler(event, context):
    try:
        #print(json.dumps(event))
        bucket =  unquote_plus(event['Records'][0]['s3']['bucket']['name'])
        key =  unquote_plus(event['Records'][0]['s3']['object']['key'])#take out of url formatting
        config = get_config(0)
        encEnabled = config['decryption']['enabled']
        print("Encryption:"+str(encEnabled))
        if encEnabled:
            enckey = getEncKey()
        split = key.split('_')
        mission_id = split[0][1:]#skip the M
        esn = split[1][1:]#skip the E
        payload_id = split[2][1:]#skip the P
        timestamp = split[3][:-4]#skip the ".bin"
        print("Getting:"+str(key))
        object = s3.get_object(Bucket=bucket, Key=key)
        data = object['Body'].read()
        header = data[:4] #first 4 bytes are timestamp, next 32 are payload
        #print("headersize="+str(len(header)))
        data = data[-32:]#strip the header, mission specific
        
        #decrypt
        if encEnabled:
            decrypter = Decrypter(AESModeOfOperationECB(enckey),PADDING_NONE)
            decrypted = decrypter.feed(data)
            decrypted += decrypter.feed()#for flushing
        else:
            decrypted = data
        #print("data=" + str(decrypted))
        #add header back on
        decrypted = header+decrypted
        print(str(decrypted))
        #put out on cosmos
        if broadcast_data(config['destination_TCP']['IPv4'], config['destination_TCP']['port'], decrypted) is None:
            print('Sent '+str(len(decrypted))+" bytes")
        else : 
            print('Failed to send')
        return 


    except ClientError as e:
        if e.response['Error']['Code'] == 'DecryptionFailureException':
            # Secrets Manager can't decrypt the protected secret text using the provided KMS key.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InternalServiceErrorException':
            # An error occurred on the server side.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidParameterException':
            # You provided an invalid value for a parameter.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'InvalidRequestException':
            # You provided a parameter value that is not valid for the current state of the resource.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
        elif e.response['Error']['Code'] == 'ResourceNotFoundException':
            # We can't find the resource that you asked for.
            # Deal with the exception here, and/or rethrow at your discretion.
            raise e
    except Exception as e:
        exc_type, exc_obj, tb = sys.exc_info()
        f = tb.tb_frame
        lineno = tb.tb_lineno
        filename = f.f_code.co_filename
        linecache.checkcache(filename)
        line = linecache.getline(filename, lineno, f.f_globals)
        print ('EXCEPTION IN ({}, LINE {} {}): {}'.format(filename, lineno, line.strip(), exc_obj))