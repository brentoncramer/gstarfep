import json
import urllib
import boto3
import linecache
import sys
from datetime import datetime
import logging
logger = logging.getLogger()
logger.setLevel(logging.INFO)
from botocore.client import Config

def get_config(id: int):
    dynamodb = boto3.resource('dynamodb')
    if dynamodb:
        table = dynamodb.Table('SSP_FEP_config')
        id = table.get_item(Key={'ConfigID':id})
        config = id['Item']
        return config
    else :
        raise Exception("Unable to get Config")
        return None;
        
s3 = boto3.client('s3', config=Config(signature_version='s3v4'))
lambda_client = boto3.client('lambda')

def invokeDecrypt(key:str, bucket:str):
  try:
    event = {
      "Records": [
        {
          "eventVersion": "2.0",
          "eventSource": "aws:s3",
          "awsRegion": "us-gov-west-1",
          "eventTime": "2019-04-08T00:00:00.000Z",
          "eventName": "ObjectCreated:Put",
          "s3": {
            "s3SchemaVersion": "1.0",
            "configurationId": "testConfigRule",
            "bucket": {
              "name": "vpm-fep",
              "ownerIdentity": {
                "principalId": "Self"
              },
              "arn": "arn:aws-us-gov:s3:::s3://vpm-fep"
            },
            "object": {
              "key": "M930_E0-3208969_P908146_2019-02-04 18:26:29.bin",
              "size": 72,
            }
          }
        }
      ]
    }
    event['Records'][0]['s3']['bucket']['name'] = bucket
    event['Records'][0]['s3']['bucket']['arn'] = "arn:aws-us-gov:s3:::s3://"+bucket
    event['Records'][0]['s3']['object']['key'] = key
    
    
    return lambda_client.invoke(
      FunctionName='VPMDecrpt',
      InvocationType='Event',
      Payload=json.dumps(event),
    )
  
  except Exception as e:
    exc_type, exc_obj, tb = sys.exc_info()
    f = tb.tb_frame
    lineno = tb.tb_lineno
    filename = f.f_code.co_filename
    linecache.checkcache(filename)
    line = linecache.getline(filename, lineno, f.f_globals)
    print ('EXCEPTION IN ({}, LINE {} {}): {}'.format(filename, lineno, line.strip(), exc_obj))
  


def lambda_handler(event, context):
  try:
      config = get_config(0)
      format = '%Y-%m-%d %H:%M:%S'
      mStart =  datetime.strptime(event['queryStringParameters']['start'],format)
      logger.info('start={}'.format(mStart))
      s3 = boto3.resource('s3')
      bucket = s3.Bucket(config['destination_S3'])
      s3.meta.client.head_bucket(Bucket=config['destination_S3'])
      mission = config['mission_id']
      esn = config['esn']
      count = 0
      for object in bucket.objects.all():
        #only our mission and esn
        key = object.key
        if "M"+ mission not in key or 'E'+esn not in key:
          continue
        timestamp = datetime.strptime(key.split('_')[3][:-4],format)#see VPMDecrpt
        if timestamp > mStart:
          invokeDecrypt(key, bucket.name)
          count = count+1
      return {
          'statusCode': 200,
          'body': '<h2>Sent {} payloads</h2>'.format(count)
      }
  except Exception as e:
      exc_type, exc_obj, tb = sys.exc_info()
      f = tb.tb_frame
      lineno = tb.tb_lineno
      filename = f.f_code.co_filename
      linecache.checkcache(filename)
      line = linecache.getline(filename, lineno, f.f_globals)
      print ('EXCEPTION IN ({}, LINE {} {}): {}'.format(filename, lineno, line.strip(), exc_obj))