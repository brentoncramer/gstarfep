#NSL Polling FEP
# NSL API access
# author: bcramer
# Created 2018 06 26

from botocore.vendored import requests
import socket
import boto3
import time

from Helpers import *



import linecache
import sys

def print_section(s: str):
    thick_line = '========================================='
    thin_line = '-----------------------------------------'
    print('\n' + thin_line)
    print('*** ' + s + ' ***')
    print(thin_line)

def login(username: str, pw: str, base_url: str, cookies: dict):
    print('Logging In')
    login_url = base_url + 'login.php?UserName=' + username + '&Password=' + pw
    r = requests.get(login_url, cookies=cookies)
    cookies = r.cookies
    return cookies

def get_simplex_data(mission_id: str, esn: str, start_payload_id: int, base_url: str, cookies: dict, catchup:bool):
    count = 100
    print('Getting Simplex Data')
    if catchup:
        while True:#break when we get more than 0
            simplex_url = base_url + 'simplex.php?MissionID=' + mission_id + '&ESN=' + esn + '&MinPayloadID=' + str(start_payload_id) + "&MaxPayloadID=" + str(start_payload_id+count)
            print('URL = ' + simplex_url)
            r = requests.get(simplex_url, cookies=cookies, timeout=1)
            simplex_records = r.json()
            if simplex_records['requestResult']:
                print("{} records recieved".format(len(simplex_records['results'])))
            else:
                print (simplex_records)
                return simplex_records['results']
            #limted to 100 entries because of Lambda function timeouts
            if len(simplex_records['results']) <= 0 and count <= 1000:
                count = count + 100
            else:
                break
        if len(simplex_records['results']) <= 0:
            #try without a max
            simplex_url = base_url + 'simplex.php?MissionID=' + mission_id + '&ESN=' + esn + '&MinPayloadID=' + str(start_payload_id)
            print('URL = ' + simplex_url)
            r = requests.get(simplex_url, cookies=cookies, timeout=1)
            simplex_records = r.json()
            if simplex_records['requestResult']:
                print("{} records recieved".format(len(simplex_records['results'])))
            else:
                print (simplex_records)
    else:
        simplex_url = base_url + 'simplex.php?MissionID=' + mission_id + '&ESN=' + esn + '&MinPayloadID=' + str(start_payload_id)
        print('URL = ' + simplex_url)
        r = requests.get(simplex_url, cookies=cookies, timeout=1)
        simplex_records = r.json()
        print("{} records recieved".format(len(simplex_records['results'])))
    return simplex_records['results']

def broadcast_data(ip_address, port, data):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    transfered = s.sendto(data.encode('UTF-8'), (ip_address, port))
    if transfered <= 0:
        raise Exception('Failed to send')


def get_config(id: int):
    dynamodb = boto3.resource('dynamodb')
    if dynamodb:
        table = dynamodb.Table('SSP_FEP_config')
        config = table.get_item(Key={'ConfigID':id})['Item']
        return config
    else :
        raise Exception("Unable to get Config")
        return None;

def save_config(id: int, config:list):
    dynamodb = boto3.resource('dynamodb')
    if dynamodb:
        table = dynamodb.Table('SSP_FEP_config')
        response = table.put_item(Item=config)
        return response
    else :
        raise Exception("Unable to get Config")
        return None;

def store_data_NoSQL(table_name:str, data):
    dynamodb = boto3.resource('dynamodb')
    if dynamodb:
        table = dynamodb.Table(table_name)
        response = table.put_item(data)
        return response
    else:
        return 'Connection Failed'

def load_decrypt_queue(dest: str, data: dict, mission_id: str, esn: str, key: str):
    try :
        sqs = boto3.resource('sqs')
        queue = sqs.Queue('https://sqs.us-gov-west-1.amazonaws.com/038969710645/FEPDecrypt')
        i=0
        for packet in data:
        #https://boto3.readthedocs.io/en/latest/reference/services/sqs.html#SQS.Queue.send_messages
            start = time.time()
            response = queue.send_message(MessageBody=packet['Payload'], MessageAttributes={
                'Mission_id':{
                    'StringValue': mission_id,
                    'DataType': 'String'
                },
                'esn':{
                    'StringValue': esn,
                    'DataType': 'String'
                },
                'PayloadID':{
                    'StringValue':packet['PayloadID'],
                    'DataType': 'String'
                },
                'DT_NSLReceived':{
                    'StringValue':packet['DT_NSLReceived'],
                    'DataType': 'String'
                },
                'key': {
                    'StringValue':key,
                    'DataType': 'String'
                }
            })
            end = time.time()
            if None != response.get('Failed'):
                raise Exception(response.get('Failed'))
            print("loaded "+ str(packet['PayloadID'])+ " in (s) " + str(end-start))

    except Exception as e:
        exc_type, exc_obj, tb = sys.exc_info()
        f = tb.tb_frame
        lineno = tb.tb_lineno
        filename = f.f_code.co_filename
        linecache.checkcache(filename)
        line = linecache.getline(filename, lineno, f.f_globals)
        print ('EXCEPTION IN ({}, LINE {} {}): {}'.format(filename, lineno, line.strip(), exc_obj))

def write_S3(data: dict, dest: str, mission_id: str, esn: str):
	s3 = boto3.resource('s3')
	bucket = s3.Bucket(dest)
	#print(bucket)
	try:
	    last:int = 0;
	    start = time.time()
	    for packet in data:
	        key = 'M{}_E{}_P{}_{}.bin'.format(mission_id, esn, packet['PayloadID'],packet['DT_NSLReceived'])
	        object = s3.Object(dest, key)
	        object.put(Body=bytes.fromhex(packet['Payload']))
	        #print("packetlen="+str(len(bytes.fromhex(packet['Payload']))))
	    end = time.time()
	    print("saved {} files in {}(s) ".format(len(data), str(end-start)))
	except Exception as e:
		print(e)
	return int(data[-1]['PayloadID'])

def read_S3(data: dict, source: str, mission_id: str, esn: str):
	S3 = boto3.resource('s3')
	bucket = s3.Bucket(source)
	try:
		s3.meta.client.head_bucket(Bucket=source)
	except Exception as e:
		print(e)
	for key in bucket.objects.all():
	    if 'M'+mission_id not in key or 'E'+esn not in key:
	        continue
	    payload_id = key.split('_P')[1]
	    object = s3.get_object(Bucket=source, Key=key)
	    data[payload_id] = object['Body']
	return True

def lambda_handler(event, context):
    cookies = dict()
    config = get_config(0)
    source = config["source"]
    if source == 'NSL':
        base_url = 'https://data2.nsldata.com/~gsdata/webAPIv1.0/'
    else :
        print("Invalid Config")
    try:
        user_name = config["user_name"]
        password = config["password"]
        mission_id = config["mission_id"]
        destination_S3 = config['destination_S3']
        esn = config["esn"]
        if 'start_payload_id' in config:
            start_payload_id = config['start_payload_id']
        else:
            start_payload_id = 0
        if 'catchup' in config:
            catchup = config['catchup']
        else: 
            catchup = (start_payload_id is 0)
        cookies = login(user_name, password, base_url, cookies)
        if 'PHPSESSID' not in cookies: #Didn't login
            raise Exception("Unable to Login to "+source)
        #missions = get_missions(base_url, cookies) only for information
        #esns = get_esns(base_url, missions, cookies) not production
        data = get_simplex_data(mission_id, esn, start_payload_id ,base_url, cookies, catchup)
        if data: #Data to find
            config["start_payload_id"] = int(write_S3(data, destination_S3, mission_id, esn) + 1)
            save_config(0,config)

            #call ourselves to make sure we get it all
            lambda_client = boto3.client('lambda')
            arn = 'arn:aws-us-gov:lambda:us-gov-west-1:717325844219:function:NSLPoller'
            response = lambda_client.invoke(FunctionName=arn,
                                 InvocationType='Event')
            print("Success")
    except Exception as e:
        exc_type, exc_obj, tb = sys.exc_info()
        f = tb.tb_frame
        lineno = tb.tb_lineno
        filename = f.f_code.co_filename
        linecache.checkcache(filename)
        line = linecache.getline(filename, lineno, f.f_globals)
        print ('EXCEPTION IN ({}, LINE {} {}): {}'.format(filename, lineno, line.strip(), exc_obj))

    finally:
        if 'PHPSESSID' in cookies :
            print('Logging Out')
            logout_url = base_url + 'logout.php'
            r = requests.get(logout_url, cookies=cookies)
            print('Response =', r.text)
        else :
            print('exiting without logging out')
    return
