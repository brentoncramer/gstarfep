def print_section(s: str):
    thick_line = '========================================='
    thin_line = '-----------------------------------------'
    print('\n' + thin_line)
    print('*** ' + s + ' ***')
    print(thin_line)




def get_missions(base_url: str, cookies: dict):
    print_section('Listing Missions')
    mission_url = base_url + 'missions.php'
    print('URL = ' + mission_url)
    r = requests.get(mission_url, cookies=cookies)
    print('Response =', r.text)
    missions = r.json()
    if missions['requestResult']:
        for mission in missions['results']:
            print(mission['MissionID'] + ': ', mission)
    else:
        print('Request result = ', missions.message)
    return missions

def get_esns(base_url: str, missions, cookies):
    # ------------------
    # Get Simplex ESNs
    # ------------------

    print_section('Getting Simplex ESNs')

    esn_url = base_url + 'ESNs.php?MissionID='
    esns = []
    for mission in missions['results']:
        mission_id = mission['MissionID']
        print('URL = ' + esn_url + mission_id)
        r = requests.get(esn_url + mission_id, cookies=cookies)
        print('Response =', r.text)
        esns = r.json()
        if esns['requestResult']:
            print('Mission ' + mission_id + ' ESNs =', end="")
            for esn in esns['results']:
                print(' ' + esn, end="")
            print()
        else:
            print('Request result = ', esns.message)
    return esns